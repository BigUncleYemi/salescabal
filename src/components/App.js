import React, { Component } from 'react';
import Slider from "react-slick";

class App extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      rtl: true
    };
    return (
      <div className="wrapper">
        <header>
          <div className="head">
            <div>Furniture</div>
            <ul>
              <li><i className="fa fa-search" /></li>
              <li><i className="fa fa-shopping-cart" /></li>
              <li><i className="fa fa-user"/></li>
            </ul>
          </div>
          <h1>Furniture <br />
            <span id="by">by</span><br />
            <span>Salescabal</span>
          </h1>
        </header>
        <div>
          <nav>
            <div>
              <ul>
                <li><a href="#chairs">Chair</a></li>
                <li><a href="#cupboards">Cupboards</a></li>
                <li><a href="#tables">Tables</a></li>
                <li><a href="#sofa">Sofa</a></li>
                <li><a href="#beds">Beds</a></li>
                <li><a href="#wardrod">Wardrod</a></li>
                <li><a href="#cabinet">Cabinet</a></li>
              </ul>
            </div>
          </nav>
          <section>
            <div className="deals">
              <div style={{ position: 'relative', width: '40%' }}>
                <div className="display">
                  <h3>Summer Deals</h3>
                  <span>0ver 30% discount</span>
                  <span>*terms and condition applied.</span>
                </div>
              </div>
              <div className="w-20">
                <div>
                  <h4>chair 1</h4>
                  <div>
                    <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                  </div>
                  <div>
                    <span>$ 200.00</span>
                  </div>
                </div>
              </div>
              <div className="w-20">
                <div>
                  <h4>chair 1</h4>
                  <div>
                    <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                  </div>
                  <div>
                    <span>$ 200.00</span>
                  </div>
                </div>
              </div>
              <div className="w-20">
                <div>
                  <h4>chair 1</h4>
                  <div>
                    <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                  </div>
                  <div>
                    <span>$ 200.00</span>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div className="catgories" id="chairs">
              <h2>Chairs</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <hr className="div"/>
            <div className="catgories" id="cupboards">
              <h2>Cupboards</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <hr className="div"/>
            <div className="catgories" id="tables">
              <h2>Tables</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>Table 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>Table 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>Table 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>Table 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>Table 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <hr className="div"/>
            <div className="catgories" id="sofa">
              <h2>Sofa</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <hr className="div"/>
            <div className="catgories" id="beds">
              <h2>Beds</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <hr className="div"/>
            <div className="catgories" id="wardrod">
              <h2>Wardrod</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div> 
            <hr className="div"/>
            <div className="catgories" id="cabinet">
              <h2>Cabinet</h2>
              <Slider {...settings} className="cat">
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
                <div className="w-20">
                  <div>
                    <h4>chair 1</h4>
                    <div>
                      <img src="http://via.placeholder.com/150x200" alt="chair 1" />
                    </div>
                    <div>
                      <span>$ 200.00</span>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
          </section>
          <section className="newsletter">
            <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center',padding: '100px'}}>
              <h3 className="sub">Subscribe to our Newsletter</h3>
              <div className="sub-inp">
                <input type="email" id="email" name="email" />
                <button>Submit</button>
              </div>
            </div>
          </section>
        </div>
        <footer>
          <div style={{display: 'flex', justifyContent: 'space-around',alignItems: 'center', margin: '30px 0'}}>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <img src="http://via.placeholder.com/50x70" alt="company logo"/>
              <span style={{padding: '10px'}}>Furnitures by Salescabal</span>
            </div>
            <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <div>
                <span>email: <a href="mailto:info@salecabel.com">Info@salecabel.com</a></span>&emsp;
                <span>tel: <a href="tel:+2348033333333">+234803-333-3333</a></span>
              </div>
              <div>
                <span>Address: 38, bbb street, aaa b/stop, Lagos, Nigeria.</span>
              </div>
            </div>
            <div>
              <span>&copy; Hostcabel Group. 2018</span>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
